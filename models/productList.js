const mongoose = require('mongoose');

const { Schema } = mongoose;

const ProductListSchema = new Schema({
  keyword: {
    type: String,
    required: true,
  },
  productList: [
    {
      title: String,
      price: Number,
      image: String,
    },
  ],
});

module.exports = mongoose.model('productList', ProductListSchema);
