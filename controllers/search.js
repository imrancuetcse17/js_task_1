const puppeteer = require("puppeteer");
const websiteModel = require("../websiteModel/websiteModel");
var axios = require("axios");
const sharp = require("sharp");
const ProductListSchema = require("../models/productList");
module.exports = {
  getSearchResult: async (req, res, next) => {
    let keyword = req.query.keyword;
    const browser = await puppeteer.launch();
    let totalProductlist = [];

    let pagePromise = (obj, url) =>
      new Promise(async (resolve, reject) => {
        const page = await browser.newPage();
        await page.goto(url);
        let productslist = await page.evaluate((obj) => {
          let list = document.querySelectorAll(obj.productDetails);
          let productList = [];
          let img, ttl;
          for (i of list) {
            try {
              img = i.getElementsByTagName(obj.imageTag)[0].src;
            } catch (error) {
              img = "No Image";
            }
            try {
              ttl = i.getElementsByClassName(obj.titleTag)[0].innerText;
            } catch (error) {
              ttl = "No title";
            }
            let a = {
              image: img,
              title: ttl,
            };
            if (i.getElementsByClassName(obj.priceTag).length != 0) {
              a["price"] = parseFloat(
                i
                  .getElementsByClassName(obj.priceTag)[0]
                  .innerText.replace("$", "")
              );
            } else {
              a["price"] = null;
            }
            productList.push(a);
          }
          return productList;
        }, obj);
        resolve(productslist);
        reject([]);
        await page.close();
      });

    for (let i = 1; i <= 6; i++) {
      let url = `https://www.amazon.com/s?k=${keyword}&page=${i}&qid=1622815316&ref=sr_pg_2`;
      let newList = await pagePromise(websiteModel.websiteModel[0], url);
      totalProductlist = totalProductlist.concat(newList);
      console.log(totalProductlist.length);
    }

    req.mydata = { totalProductlist: await totalProductlist, keyword: keyword };
    next();
    await browser.close();
  },

  process: async (req, res, next) => {
    let { totalProductlist, keyword } = req.mydata;
    totalProductlist = totalProductlist.splice(0, 100);
    for (i of totalProductlist) {
      let image = await axios.get(i.image, { responseType: "arraybuffer" });
      let raw = Buffer.from(image.data).toString("base64");
      let base64Image =
        (await "data:") + image.headers["content-type"] + ";base64," + raw;
      let parts = base64Image.split(";");
      let mimType = parts[0].split(":")[1];
      let imageData = parts[1].split(",")[1];

      var img = new Buffer(imageData, "base64");
      let resizedImageData = await sharp(img).resize(256, 256).toBuffer();
      resizedImageData = resizedImageData.toString("base64");
      i.image = `data:${mimType};base64,${resizedImageData}`;
    }

    const productslist = new ProductListSchema({
      keyword: keyword,
      productList: await totalProductlist,
    });

    productslist
      .save()
      .then((result) => {
        res.send(result);
      })
      .catch((err) => {
        res.send(err);
      });
  },

  index: (req, res, next) => {
    res.json({ message: "welcome to web scraper" });
  },
};
