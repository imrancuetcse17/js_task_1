module.exports = {
  websiteModel: [
    {
      productDetails: 'div[class="a-section a-spacing-medium"]',
      imageTag: "img",
      priceTag: "a-offscreen",
      titleTag: "a-link-normal a-text-normal",
    },
  ],
};
