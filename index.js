const express = require("express");
const search = require("./routes/search");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();
const app = express();

app.use(express.json());

app.use(search);

mongoose
  .connect(process.env.mongoUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("database connected");
  })
  .catch(() => {
    throw err;
  });

app.listen(process.env.PORT | 4000, () => {
  console.log("server started");
});
