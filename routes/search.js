const express = require('express');

const controller = require('../controllers/search');

const router = express.Router();

router.route('/api/search').get(controller.getSearchResult,controller.process);
router.route('/').get(controller.index);

module.exports = router;